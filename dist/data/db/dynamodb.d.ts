import AWS, { DynamoDB } from 'aws-sdk';
import { EthereumAddress, TokenId } from '@altitude-games/alpaca-web3';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { ChainType } from '../../types';
export declare const METADATA_TABLE: string;
export declare const ACCOUNTS_TABLE: string;
export declare const BATTLERACERS_TABLE: string;
export declare const SERIALS_TABLE: string;
declare class DynamoDBInterface {
    private __logger;
    private __awsService;
    private __docClient;
    constructor();
    get service(): DynamoDB;
    get client(): DynamoDB.DocumentClient;
    getMetadata(tokenIds: Array<TokenId> | TokenId, raw?: boolean): Promise<false | DynamoDB.DocumentClient.AttributeMap>;
    putMetadata(jsonData: DocumentClient.AttributeMap): Promise<import("aws-sdk/lib/request").PromiseResult<DynamoDB.DocumentClient.PutItemOutput, AWS.AWSError>>;
    getSerialNum(id: string): Promise<any>;
    createMetadata(jsonData: DocumentClient.AttributeMap, serialPartId: string, count: string | number): Promise<import("aws-sdk/lib/request").PromiseResult<DynamoDB.DocumentClient.BatchWriteItemOutput, AWS.AWSError>>;
    commitMetadataOwner(id: TokenId): Promise<import("aws-sdk/lib/request").PromiseResult<DynamoDB.DocumentClient.UpdateItemOutput, AWS.AWSError>>;
    getMetadataOwnedCount(owner: EthereumAddress, chain: ChainType): Promise<number | undefined>;
    updateAccountPartsCount(ethWallet: EthereumAddress): Promise<{
        sideChainCount: number | undefined;
        mainChainCount: number | undefined;
    }>;
    /**
     * Set
     *  txHash = 0 if state should not be altered.
     *  txHash = null if state needs to be cleared as READY
     *  txHash = actual transaction hash if needs to be monitored as pending
     */
    prepMetadataTransfer(id: TokenId, owner: EthereumAddress, crateType?: string, txHash?: string | number | null): Promise<import("aws-sdk/lib/request").PromiseResult<DynamoDB.DocumentClient.UpdateItemOutput, AWS.AWSError>>;
}
export default class AWSDynamoDB {
    static instance: DynamoDBInterface;
    constructor();
    static getInstance(): DynamoDBInterface;
}
export {};
