"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SERIALS_TABLE = exports.BATTLERACERS_TABLE = exports.ACCOUNTS_TABLE = exports.METADATA_TABLE = void 0;
const aws_sdk_1 = __importDefault(require("aws-sdk"));
const bunyan_1 = __importDefault(require("bunyan"));
const types_1 = require("../../types");
if (!process.env.DYNAMO_TABLE_NAMESPACE || !process.env.AWS_ACCESS_KEY_ID || !process.env.AWS_SECRET_ACCESS_KEY) {
    require('dotenv').config();
}
const NAMESPACE = process.env.DYNAMO_TABLE_NAMESPACE || "";
exports.METADATA_TABLE = NAMESPACE + "TokenMetadata";
exports.ACCOUNTS_TABLE = NAMESPACE + "Accounts";
exports.BATTLERACERS_TABLE = NAMESPACE + "BattleRacers";
exports.SERIALS_TABLE = NAMESPACE + "SerialNumbers";
class DynamoDBInterface {
    constructor() {
        this.__logger = bunyan_1.default.createLogger({ name: 'dynamodb', level: (process.env.NODE_ENV === "development" ? "debug" : "info") });
        const AWS_REGION = process.env.AWS_REGION || 'ap-southeast-1';
        const AWS_API_VERSION = process.env.AWS_API_VERSION || '2012-08-10';
        const AWS_ACCESS_KEY_ID = process.env.AWS_ACCESS_KEY_ID;
        const AWS_SECRET_ACCESS_KEY = process.env.AWS_SECRET_ACCESS_KEY;
        const dynamoDbOpts = { region: AWS_REGION, apiVersion: AWS_API_VERSION, accessKeyId: AWS_ACCESS_KEY_ID, secretAccessKey: AWS_SECRET_ACCESS_KEY };
        if (process.env.DYNAMO_ENDPOINT) {
            dynamoDbOpts.endpoint = process.env.DYNAMO_ENDPOINT;
            this.__logger.info("Using development DynamoDB Connection", process.env.DYNAMO_ENDPOINT);
        }
        else {
            this.__logger.info("Using a live DynamoDB Connection");
        }
        this.__awsService = new aws_sdk_1.default.DynamoDB(dynamoDbOpts);
        this.__docClient = new aws_sdk_1.default.DynamoDB.DocumentClient({ service: this.__awsService });
    }
    get service() {
        return this.__awsService;
    }
    get client() {
        return this.__docClient;
    }
    async getMetadata(tokenIds, raw = false) {
        const metadataIds = tokenIds instanceof Array ? tokenIds : [tokenIds];
        if (metadataIds.length === 0) {
            return [];
        }
        const params = {
            RequestItems: {}
        };
        params.RequestItems[exports.METADATA_TABLE] = {
            Keys: []
        };
        if (!raw) {
            this.__logger.info("getMetadata not raw");
            params.RequestItems[exports.METADATA_TABLE].ExpressionAttributeNames = {
                "#name": "name",
                "#state": "state"
            };
            params.RequestItems[exports.METADATA_TABLE].ProjectionExpression = "id, #name, description, details, image, external_url, chain, #state";
        }
        for (var i = 0; i < metadataIds.length; i++) {
            params.RequestItems[exports.METADATA_TABLE].Keys.push({ id: metadataIds[i] });
        }
        const resp = await this.client.batchGet(params).promise();
        const items = !resp.Responses ? [] : resp.Responses[exports.METADATA_TABLE];
        return metadataIds.length == 1 ? (items.length > 0 ? items[0] : false) : items;
    }
    putMetadata(jsonData) {
        var params = {
            TableName: exports.METADATA_TABLE,
            Item: jsonData
        };
        return this.client.put(params).promise();
    }
    async getSerialNum(id) {
        var params = {
            TableName: exports.SERIALS_TABLE,
            Key: { id }
        };
        var resp = await this.client.get(params).promise();
        var item = resp.Item;
        return (item && item.count) || "0";
    }
    async createMetadata(jsonData, serialPartId, count) {
        var params = { RequestItems: {} };
        params.RequestItems[exports.SERIALS_TABLE] = [
            {
                PutRequest: {
                    Item: {
                        id: serialPartId,
                        count: count
                    }
                }
            }
        ];
        params.RequestItems[exports.METADATA_TABLE] = [
            {
                PutRequest: {
                    Item: jsonData
                }
            }
        ];
        return this.client.batchWrite(params).promise();
    }
    async commitMetadataOwner(id) {
        var params = {
            TableName: exports.METADATA_TABLE,
            Key: { id },
            UpdateExpression: "SET #owner = transferTo, transferComplete = :isComplete, #state = :state REMOVE pendingTxnHash",
            ExpressionAttributeNames: {
                "#state": "state",
                "#owner": "owner"
            },
            ExpressionAttributeValues: {
                ':isComplete': types_1.YES,
                ':state': types_1.PART_STATE_READY
            }
        };
        return this.client.update(params).promise();
    }
    async getMetadataOwnedCount(owner, chain) {
        var params = {
            TableName: exports.METADATA_TABLE,
            IndexName: "owner-chainSortId-index",
            ReturnConsumedCapacity: "INDEXES",
            KeyConditionExpression: "#owner = :owner",
            ExpressionAttributeNames: {
                "#owner": "owner"
            },
            ExpressionAttributeValues: {
                ':owner': owner.$
            },
            Select: 'COUNT'
        };
        if (chain !== "ALL") {
            params.FilterExpression = "chain = :chain";
            params.ExpressionAttributeValues[":chain"] = chain;
        }
        var queryResult = await this.client.query(params).promise();
        return queryResult.Count;
    }
    async updateAccountPartsCount(ethWallet) {
        const sideChainCount = await this.getMetadataOwnedCount(ethWallet, types_1.SIDECHAIN);
        const mainChainCount = await this.getMetadataOwnedCount(ethWallet, types_1.ETHEREUM);
        var params = {
            TableName: exports.ACCOUNTS_TABLE,
            Key: { ethWallet: ethWallet.$ },
            UpdateExpression: "set sideChainParts = :sideChainParts, mainChainParts = :mainChainParts",
            ExpressionAttributeValues: {
                ':sideChainParts': sideChainCount,
                ':mainChainParts': mainChainCount
            },
            ReturnValues: "UPDATED_NEW"
        };
        await this.client.update(params).promise();
        return { sideChainCount, mainChainCount };
    }
    /**
     * Set
     *  txHash = 0 if state should not be altered.
     *  txHash = null if state needs to be cleared as READY
     *  txHash = actual transaction hash if needs to be monitored as pending
     */
    async prepMetadataTransfer(id, owner, crateType, txHash) {
        var params = {
            TableName: exports.METADATA_TABLE,
            Key: { id },
            UpdateExpression: "set transferTo = :owner, transferTime = :transferTime",
            ExpressionAttributeValues: {
                ':owner': owner.$,
                ':transferTime': Date.now()
            }
        };
        if (crateType) {
            params.UpdateExpression = `${params.UpdateExpression}, crateBox = :crateBox`;
            params.ExpressionAttributeValues[':crateBox'] = crateType;
        }
        if (txHash && txHash !== 0) {
            params.UpdateExpression = `${params.UpdateExpression}, pendingTxnHash = :txHash, transferComplete = :isComplete, #state = :state`;
            params.ExpressionAttributeValues[':txHash'] = txHash;
            params.ExpressionAttributeValues[':isComplete'] = types_1.NO; //will be updated by partsTransferDaemon
            params.ExpressionAttributeValues[':state'] = types_1.PART_STATE_TRANSIT;
            params.ExpressionAttributeNames = {
                '#state': 'state'
            };
        }
        else if (txHash === null) {
            params.UpdateExpression = `${params.UpdateExpression}, pendingTxnHash = :txHash, transferComplete = :isComplete, #state = :state, #owner = :owner`;
            params.ExpressionAttributeValues[':isComplete'] = types_1.YES;
            params.ExpressionAttributeValues[':txHash'] = "";
            params.ExpressionAttributeValues[':state'] = types_1.PART_STATE_READY;
            params.ExpressionAttributeNames = {
                '#state': 'state',
                '#owner': "owner"
            };
        }
        else { //txHash == 0
            params.UpdateExpression = `${params.UpdateExpression}, #owner = :owner`;
            params.ExpressionAttributeNames = {
                '#owner': "owner"
            };
        }
        return this.client.update(params).promise();
    }
}
class AWSDynamoDB {
    constructor() {
        throw new Error('Use DynamoDB.getInstance()');
    }
    static getInstance() {
        if (!AWSDynamoDB.instance) {
            AWSDynamoDB.instance = new DynamoDBInterface();
        }
        return AWSDynamoDB.instance;
    }
}
exports.default = AWSDynamoDB;
