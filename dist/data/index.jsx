"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BATTLERACERS_TABLE = exports.ACCOUNTS_TABLE = exports.METADATA_TABLE = exports.AWSDynamoDB = void 0;
var dynamodb_1 = require("./db/dynamodb");
Object.defineProperty(exports, "AWSDynamoDB", { enumerable: true, get: function () { return __importDefault(dynamodb_1).default; } });
Object.defineProperty(exports, "METADATA_TABLE", { enumerable: true, get: function () { return dynamodb_1.METADATA_TABLE; } });
Object.defineProperty(exports, "ACCOUNTS_TABLE", { enumerable: true, get: function () { return dynamodb_1.ACCOUNTS_TABLE; } });
Object.defineProperty(exports, "BATTLERACERS_TABLE", { enumerable: true, get: function () { return dynamodb_1.BATTLERACERS_TABLE; } });
