"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BattleCarPart = void 0;
var BattleCarPart_1 = require("./BattleCarPart");
Object.defineProperty(exports, "BattleCarPart", { enumerable: true, get: function () { return __importDefault(BattleCarPart_1).default; } });
