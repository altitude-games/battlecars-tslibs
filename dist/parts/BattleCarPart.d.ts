import Bunyan from "bunyan";
import { CarPart, EthereumAddress, TokenId } from "@altitude-games/alpaca-web3";
import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { CarPartMetadata, CarPartMetadataDetails } from "../types/CarPartMetadata";
import { ChainType } from "../types";
export default class BattleCarPart implements CarPartMetadata {
    private __dynamodb;
    private __mainContract;
    private __sideContract;
    private __metadata;
    protected _logger: Bunyan;
    constructor(mainContract: CarPart, sideContract: CarPart, carPartId?: TokenId);
    balanceOf(wallet: EthereumAddress, chain?: ChainType): Promise<number>;
    get id(): TokenId;
    get name(): string;
    get description(): string;
    get image(): string;
    get external_url(): string;
    get details(): CarPartMetadataDetails;
    get metadata(): DocumentClient.AttributeMap | false;
    set metadata(metadata: DocumentClient.AttributeMap | false);
    loadMetadata(tokenId: TokenId): Promise<false | DocumentClient.AttributeMap>;
    isVerifiedOwner(): Promise<boolean>;
    save(destinationWallet: EthereumAddress, crateBox?: string): Promise<void>;
    private __onPartTransactionCompleted;
}
