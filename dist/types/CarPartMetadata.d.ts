import { CarPartEdition, CarPartRarity } from ".";
import { TokenId } from "@altitude-games/alpaca-web3";
export interface CarPartMetadataDetails {
    internalId: string;
    edition: CarPartEdition;
    brand: string;
    model: string;
    internalType: string;
    type: string;
    rarity: CarPartRarity;
    weight: number;
    durability: number;
    speed: number;
    power: number;
    steering: number;
    isElite?: boolean;
}
export interface CarPartMetadata {
    id?: TokenId;
    name: string;
    description: string;
    image: string;
    external_url?: string;
    details: CarPartMetadataDetails;
    provisioningId?: string;
}
export interface LegendaryPartMarker {
    isLegendary: boolean;
}
