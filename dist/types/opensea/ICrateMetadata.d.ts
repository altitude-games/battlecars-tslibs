export default interface OSCrateMetadata {
    metaName: string;
    metaDescription: string;
    metaMarkdownDescription?: string;
    metaImage: string;
    metaIcon: string;
    metaExternalUrl: string;
    metaAnimationUrl: string;
    metaYoutubeUrl: string;
    metaAttributes: Array<{
        [attr: string]: string | number | boolean;
    }>;
}
