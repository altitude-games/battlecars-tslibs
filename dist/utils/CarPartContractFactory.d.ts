import { CarPart, ChainConfigOverride, ChainNetConfig } from "@altitude-games/alpaca-web3";
import { ChainType } from "../types";
export default class CarPartContractFactory {
    static instances: {
        [chain: string]: CarPart;
    };
    constructor();
    static getInstance(chain: ChainType, networkConfig: ChainNetConfig, overrides?: ChainConfigOverride): CarPart;
}
