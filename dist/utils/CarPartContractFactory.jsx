"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const alpaca_web3_1 = require("@altitude-games/alpaca-web3");
const crypto_1 = __importDefault(require("crypto"));
const types_1 = require("../types");
class EthereumCarPart extends alpaca_web3_1.CarPart {
    constructor(networkConfig, overrides) {
        super("Part", networkConfig, overrides);
    }
}
class SidechainCarPart extends alpaca_web3_1.CarPart {
    constructor(networkConfig, overrides) {
        super("BRPart", networkConfig, overrides);
    }
}
class CarPartContractFactory {
    constructor() {
        throw new Error("Use EthereumCarPartInstantiator.getCarPartContract()");
    }
    static getInstance(chain, networkConfig, overrides) {
        const hashKey = crypto_1.default.createHash('md5').update(`${JSON.stringify(networkConfig)}${!!overrides ? JSON.stringify(overrides) : ""}`).digest('hex');
        const mappingKey = `${chain}#${hashKey}`;
        if (!CarPartContractFactory.instances[mappingKey]) {
            if (chain === types_1.ETHEREUM) {
                CarPartContractFactory.instances[mappingKey] = new EthereumCarPart(networkConfig, overrides);
            }
            else {
                CarPartContractFactory.instances[mappingKey] = new SidechainCarPart(networkConfig, overrides);
            }
        }
        return CarPartContractFactory.instances[mappingKey];
    }
}
exports.default = CarPartContractFactory;
CarPartContractFactory.instances = {};
