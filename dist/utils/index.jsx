"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CarPartContractFactory = void 0;
var CarPartContractFactory_1 = require("./CarPartContractFactory");
Object.defineProperty(exports, "CarPartContractFactory", { enumerable: true, get: function () { return __importDefault(CarPartContractFactory_1).default; } });
