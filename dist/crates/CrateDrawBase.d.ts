import { CarPart, ChainConfigOverride, ChainNetConfig, Crate, EthereumAddress, TokenId } from "@altitude-games/alpaca-web3";
import { CarPartMetadataPrototype, CarPartRarity, DropRateConfig } from "../types";
import { CarPartMetadata, LegendaryPartMarker } from "../types/CarPartMetadata";
import { BattleCarPart } from "../parts";
export default class CrateDrawBase extends Crate {
    protected _sourceAddress: EthereumAddress | null;
    protected _partsMetadataDB: Array<any>;
    constructor(contractName: string, ethNetworkConfig: ChainNetConfig, overrides?: ChainConfigOverride);
    set partsAccount(wallet: EthereumAddress | null);
    get partsAccount(): EthereumAddress | null;
    set partsMetadataDB(partsMetadataDB: Array<any>);
    get partsMetadataDB(): Array<any>;
    protected _getLegendaryRate(config: DropRateConfig): number;
    protected _getPartsByRarity(rarity: CarPartRarity): Array<CarPartMetadataPrototype>;
    protected _drawPart(rarity: CarPartRarity): CarPartMetadata;
    protected _performCrateDraw(config: DropRateConfig, hasLegendaries: boolean): Array<CarPartMetadata | LegendaryPartMarker>;
    protected _drawLegendary(partContract: CarPart, sourceAccount: EthereumAddress): Promise<TokenId | false>;
    /**
     * Open crate and yield parts, either pre-existing or to be minted.
     * @param config drop rate configuration
     * @param partContract contract connecting to side chain -- e.g. Matic as of this moment
     * @param partsSourceAccount source wallet that owns legendary parts
     */
    protected _openCrate(config: DropRateConfig, partContract: CarPart, partsSourceAccount: EthereumAddress): Promise<Array<BattleCarPart>>;
}
