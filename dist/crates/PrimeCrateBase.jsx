"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const alpaca_web3_1 = require("@altitude-games/alpaca-web3");
const data_json_1 = __importDefault(require("../data/json/data.json"));
const CrateDrawBase_1 = __importDefault(require("./CrateDrawBase"));
class PrimeCrateBase extends CrateDrawBase_1.default {
    constructor(contractName, ethNetworkConfig, overrides) {
        super(contractName, ethNetworkConfig, overrides);
        this.partsMetadataDB = data_json_1.default.Parts;
        const partsMnemonic = process.env.PARTS_MNEMONIC;
        if (partsMnemonic) {
            alpaca_web3_1.mnemonicConverter(partsMnemonic, 0, 4).then(addresses => {
                this._sourceAddress = new alpaca_web3_1.EthereumAddress(addresses[0]);
            });
        }
    }
    /**
     * Delete this function no hardcoded values are no longer necessary
     * @param partContract
     * @param sourceAccount
     */
    async _drawLegendary(partContract, sourceAccount) {
        //number of legendaries according to old code
        //does this means we only have 61 legendaries 
        //starting from token 1 to 61?
        const magicNumber = 61;
        const remaining = await partContract.balanceOf(sourceAccount);
        if (remaining > 0) {
            return magicNumber - remaining;
        }
        else {
            return false;
        }
    }
}
exports.default = PrimeCrateBase;
