"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const alpaca_web3_1 = require("@altitude-games/alpaca-web3");
const parts_1 = require("../parts");
const types_1 = require("../types");
const CarPartContractFactory_1 = __importDefault(require("../utils/CarPartContractFactory"));
class CratePoolDrawBase extends alpaca_web3_1.Crate {
    constructor(contractName, ethNetworkConfig, overrides) {
        super(contractName, ethNetworkConfig, overrides);
        this._sourceAddress = null;
    }
    set partsAccount(wallet) {
        this._sourceAddress = wallet;
    }
    get partsAccount() {
        return this._sourceAddress;
    }
    async __drawPart(partContract, partsSourceAccount) {
        this._logger.debug(`[${this.contractName}].__drawPart Drawing part from ${await partContract.tokenName()} from ${partsSourceAccount.$} balances.`);
        const sourceBalance = await partContract.balanceOf(partsSourceAccount);
        this._logger.debug(`[${this.contractName}].__drawPart ${await partContract.tokenName()} Balance: ${sourceBalance}`);
        const roll = Math.floor(Math.random() * Number(sourceBalance));
        const tokenId = await partContract.tokenOfOwnerByIndex(partsSourceAccount, roll);
        this._logger.debug(`[${this.contractName}].__drawPart Roll: ${roll}, TokenID: ${tokenId}`);
        return tokenId;
    }
    async _drawParts(partContract, partsSourceAccount, count = 1) {
        this._logger.debug(`[${this.contractName}].drawParts Drawing ${count} ${await partContract.tokenName()} from ${partsSourceAccount}`);
        const sourceBalance = await partContract.balanceOf(partsSourceAccount);
        if (sourceBalance === 0 || sourceBalance < count) {
            throw new alpaca_web3_1.InsufficientCarPartsError(`Not enough tokens for drawing from ${await partContract.tokenName()} under the source account ${partsSourceAccount.$}`);
        }
        const checkList = [];
        const drawnParts = [];
        while (drawnParts.length < count) {
            const tokenDrawn = await this.__drawPart(partContract, partsSourceAccount);
            if (checkList.indexOf(tokenDrawn) < 0) {
                checkList.push(tokenDrawn);
                const ethereumPartContract = CarPartContractFactory_1.default.getInstance(types_1.ETHEREUM, this.networkConfig, //create is deployed on ethereum, we will re-use its config to request part contract instance of ethereum
                this._artifacts.overrides);
                const battleCarPart = new parts_1.BattleCarPart(ethereumPartContract, partContract);
                await battleCarPart.loadMetadata(tokenDrawn);
                drawnParts.push(battleCarPart);
            }
        }
        return drawnParts;
    }
}
exports.default = CratePoolDrawBase;
