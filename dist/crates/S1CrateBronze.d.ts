import { ChainNetConfig, ChainConfigOverride, CarPart, EthereumAddress } from "@altitude-games/alpaca-web3";
import { ICrate, CrateEdition, CrateRarity, DropRateConfig } from "../types";
import S1CrateBase from "./S1CrateBase";
import { BattleCarPart } from "../parts";
export declare const S1_BRONZE_DROP_RATE: DropRateConfig;
export default class S1CrateBronze extends S1CrateBase implements ICrate {
    constructor(ethNetworkConfig: ChainNetConfig, overrides?: ChainConfigOverride);
    get name(): string;
    get code(): string;
    get description(): string;
    get image(): string;
    get edition(): CrateEdition;
    get rarity(): CrateRarity;
    get metaName(): string;
    get metaDescription(): string;
    get metaMarkdownDescription(): string;
    get metaImage(): string;
    get metaIcon(): string;
    get metaExternalUrl(): string;
    get metaAnimationUrl(): string;
    get metaYoutubeUrl(): string;
    get metaAttributes(): Array<{
        [attr: string]: string | number | boolean;
    }>;
    /**
     * Open crate and yield parts, either pre-existing or to be minted.
     * @param partContract contract connecting to side chain -- e.g. Matic as of this moment
     * @param partsSourceAccount source wallet that owns legendary parts
     */
    openCrate(partContract: CarPart, partsSourceAccount: EthereumAddress): Promise<Array<BattleCarPart> | false>;
}
