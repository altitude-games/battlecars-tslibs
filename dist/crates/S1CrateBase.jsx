"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const alpaca_web3_1 = require("@altitude-games/alpaca-web3");
const s1_data_json_1 = __importDefault(require("../data/json/s1_data.json"));
const CrateDrawBase_1 = __importDefault(require("./CrateDrawBase"));
class S1CrateBase extends CrateDrawBase_1.default {
    constructor(contractName, ethNetworkConfig, overrides) {
        super(contractName, ethNetworkConfig, overrides);
        this.partsMetadataDB = s1_data_json_1.default.Parts;
        const partsMnemonic = process.env.PARTS_MNEMONIC;
        if (partsMnemonic) {
            alpaca_web3_1.mnemonicConverter(partsMnemonic, 0, 4).then(addresses => {
                this._sourceAddress = new alpaca_web3_1.EthereumAddress(addresses[2]);
            });
        }
    }
    /**
     * Delete this function no hardcoded values are no longer necessary
     * @param partContract
     * @param sourceAccount
     */
    async _drawLegendary(partContract, sourceAccount) {
        let partObtained = false;
        const magicNumber = 20; //number of legendaries
        const remaining = await partContract.balanceOf(sourceAccount);
        if (remaining > 0) {
            const diff = magicNumber - remaining;
            const startTokenId = Number(process.env.S1_LEG_START);
            return startTokenId + diff;
        }
        else {
            return false;
        }
        return partObtained;
    }
}
exports.default = S1CrateBase;
