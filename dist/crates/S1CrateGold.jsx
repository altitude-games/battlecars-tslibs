"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.S1_GOLD_DROP_RATE = void 0;
const S1CrateBase_1 = __importDefault(require("./S1CrateBase"));
exports.S1_GOLD_DROP_RATE = {
    legendary: 0.014423,
    elite: 0.015,
    parts: 4,
    rates: [0.20, 0.50, 0.30] // common, rare, epic
};
class S1CrateGold extends S1CrateBase_1.default {
    constructor(ethNetworkConfig, overrides) {
        super('S1CrateGold', ethNetworkConfig, overrides);
    }
    get name() {
        return "Season 1 Gold Crate";
    }
    get code() {
        return "S1CrateGold";
    }
    get description() {
        return "Contains 4 parts from the Season 1 collection. This crate has the highest chance to drop Epic parts. More info at https://battleracers.io.";
    }
    get image() {
        return "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/S01Gold.png";
    }
    get edition() {
        return "Season 1";
    }
    get rarity() {
        return "Gold";
    }
    get metaName() {
        return this.name;
    }
    get metaDescription() {
        return this.description;
    }
    get metaMarkdownDescription() {
        return "Contains 4 parts from the Season 1 collection. This crate has the highest chance to drop Epic parts. More info at [visible text](https://battleracers.io).";
    }
    get metaImage() {
        return this.image;
    }
    get metaIcon() {
        return "";
    }
    get metaExternalUrl() {
        return "";
    }
    get metaAnimationUrl() {
        return "";
    }
    get metaYoutubeUrl() {
        return "";
    }
    get metaAttributes() {
        return [
            {
                "trait_type": "Edition",
                "value": "Season 1"
            },
            {
                "trait_type": "Rarity",
                "value": "Gold"
            }
        ];
    }
    /**
     * Open crate and yield parts, either pre-existing or to be minted.
     * @param partContract contract connecting to side chain -- e.g. Matic as of this moment
     * @param partsSourceAccount source wallet that owns legendary parts
     */
    async openCrate(partContract, partsSourceAccount) {
        if (!partsSourceAccount && this.partsAccount) {
            return await this._openCrate(exports.S1_GOLD_DROP_RATE, partContract, this.partsAccount);
        }
        else if (partsSourceAccount) {
            return await this._openCrate(exports.S1_GOLD_DROP_RATE, partContract, partsSourceAccount);
        }
        else {
            return false;
        }
    }
}
exports.default = S1CrateGold;
