"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BRONZE_DROP_RATE = void 0;
const PrimeCrateBase_1 = __importDefault(require("./PrimeCrateBase"));
exports.BRONZE_DROP_RATE = {
    legendary: 0.001935,
    elite: 0.03,
    parts: 2,
    rates: [0.67, 0.28, 0.05] // common, rare, epic
};
class CrateBronze extends PrimeCrateBase_1.default {
    constructor(ethNetworkConfig, overrides) {
        super('CrateBronze', ethNetworkConfig, overrides);
    }
    get name() {
        return "Prime Bronze Crate";
    }
    get code() {
        return "CrateBronze";
    }
    get description() {
        return "Prime Crate from Battle Racers\n\n" +
            "Contains 2 parts from the Prime collection. This crate has a high chance to drop Common parts.\n\n" +
            "More info at battleracers.io.";
    }
    get image() {
        return "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/CrateBronzeShiny.png";
    }
    get edition() {
        return "Prime";
    }
    get rarity() {
        return "Bronze";
    }
    get metaName() {
        return this.name;
    }
    get metaDescription() {
        return this.description;
    }
    get metaMarkdownDescription() {
        return "Prime Crate from Battle Racers\n\n" +
            "Contains 2 parts from the Prime collection. This crate has a high chance to drop Common parts.\n\n" +
            "More info at [visible text](https://battleracers.io).";
    }
    get metaImage() {
        return this.image;
    }
    get metaIcon() {
        return "";
    }
    get metaExternalUrl() {
        return "";
    }
    get metaAnimationUrl() {
        return "";
    }
    get metaYoutubeUrl() {
        return "";
    }
    get metaAttributes() {
        return [
            {
                "trait_type": "Edition",
                "value": "Prime"
            },
            {
                "trait_type": "Rarity",
                "value": "Bronze"
            }
        ];
    }
    /**
     * Open crate and yield parts, either pre-existing or to be minted.
     * @param partContract contract connecting to side chain -- e.g. Matic as of this moment
     * @param partsSourceAccount source wallet that owns legendary parts
     */
    async openCrate(partContract, partsSourceAccount) {
        if (!partsSourceAccount && this.partsAccount) {
            return await this._openCrate(exports.BRONZE_DROP_RATE, partContract, this.partsAccount);
        }
        else if (partsSourceAccount) {
            return await this._openCrate(exports.BRONZE_DROP_RATE, partContract, partsSourceAccount);
        }
        else {
            return false;
        }
    }
}
exports.default = CrateBronze;
