"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const alpaca_web3_1 = require("@altitude-games/alpaca-web3");
const CratePoolDrawBase_1 = __importDefault(require("./CratePoolDrawBase"));
class CrateAxie extends CratePoolDrawBase_1.default {
    constructor(ethNetworkConfig, overrides) {
        super('CrateAxie', ethNetworkConfig, overrides);
        const partsMnemonic = process.env.PARTS_MNEMONIC;
        if (partsMnemonic) {
            alpaca_web3_1.mnemonicConverter(partsMnemonic, 0, 4).then(addresses => {
                this._sourceAddress = new alpaca_web3_1.EthereumAddress(addresses[1]);
            });
        }
    }
    get name() {
        return "Axie Infinity Crate";
    }
    get code() {
        return "CrateAxie";
    }
    get description() {
        return "Axie Infinity Crate from Battle Racers\n\n" +
            "Contains 4 parts, each from the limited edition sets of Chubby Carrot, Sleepless Bite, and Triple Nut Star.\n\n" +
            "More info at battleracers.io.";
    }
    get image() {
        return "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/CrateAxie.png";
    }
    get edition() {
        return "Axie";
    }
    get rarity() {
        return "";
    }
    get metaName() {
        return this.name;
    }
    get metaDescription() {
        return this.description;
    }
    get metaMarkdownDescription() {
        return "Axie Infinity Crate from Battle Racers\n\n" +
            "Contains 4 parts, each from the limited edition sets of Chubby Carrot, Sleepless Bite, and Triple Nut Star.\n\n" +
            "More info at [visible text](https://battleracers.io)";
    }
    get metaImage() {
        return this.image;
    }
    get metaIcon() {
        return "";
    }
    get metaExternalUrl() {
        return "";
    }
    get metaAnimationUrl() {
        return "";
    }
    get metaYoutubeUrl() {
        return "";
    }
    get metaAttributes() {
        return [
            {
                "trait_type": "Edition",
                "value": "Axie"
            }
        ];
    }
    async setErc721ContractAddress(erc721ContractAddr) {
        this.contract.methods.setErc721ContractAddress(erc721ContractAddr).send();
    }
    async openCrate(partContract, partsSourceAccount) {
        try {
            return await this._drawParts(partContract, partsSourceAccount, 4);
        }
        catch (error) {
            this._logger.error(error);
            return false;
        }
    }
}
exports.default = CrateAxie;
