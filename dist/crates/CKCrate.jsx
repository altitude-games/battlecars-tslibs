"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const alpaca_web3_1 = require("@altitude-games/alpaca-web3");
const CratePoolDrawBase_1 = __importDefault(require("./CratePoolDrawBase"));
class CKCrate extends CratePoolDrawBase_1.default {
    constructor(ethNetworkConfig, overrides) {
        super('CKCrate', ethNetworkConfig, overrides);
        const partsMnemonic = process.env.PARTS_MNEMONIC;
        if (partsMnemonic) {
            alpaca_web3_1.mnemonicConverter(partsMnemonic, 0, 4).then(addresses => {
                this._sourceAddress = new alpaca_web3_1.EthereumAddress(addresses[3]);
            });
        }
    }
    get name() {
        return "CryptoKitties Crate";
    }
    get code() {
        return "CKCrate";
    }
    get description() {
        return "CryptoKitties Crate from Battle Racers\n\n" +
            "Contains 2 parts, each from the limited edition sets of RoboKitty MK-I and RoboKitty MK-II.\n\n" +
            "More info at battleracers.io.";
    }
    get image() {
        return "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/CrateCK.png";
    }
    get edition() {
        return "Cryptokitties";
    }
    get rarity() {
        return "";
    }
    get metaName() {
        return this.name;
    }
    get metaDescription() {
        return this.description;
    }
    get metaMarkdownDescription() {
        return "CryptoKitties Crate from Battle Racers\n" +
            "Contains 2 parts, each from the limited edition sets of RoboKitty MK-I and RoboKitty MK-II.\n\n" +
            "More info at [visible text](https://battleracers.io).";
    }
    get metaImage() {
        return this.image;
    }
    get metaIcon() {
        return "";
    }
    get metaExternalUrl() {
        return "";
    }
    get metaAnimationUrl() {
        return "";
    }
    get metaYoutubeUrl() {
        return "";
    }
    get metaAttributes() {
        return [
            {
                "trait_type": "Edition",
                "value": "CryptoKitties"
            }
        ];
    }
    async openCrate(partContract, partsSourceAccount) {
        try {
            return await this._drawParts(partContract, partsSourceAccount, 2);
        }
        catch (error) {
            this._logger.error(error);
            return false;
        }
    }
}
exports.default = CKCrate;
