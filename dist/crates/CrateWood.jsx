"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WOOD_DROP_RATE = void 0;
const PrimeCrateBase_1 = __importDefault(require("./PrimeCrateBase"));
exports.WOOD_DROP_RATE = {
    legendary: 0.000556,
    elite: 0.03,
    parts: 1,
    rates: [0.80, 0.17, 0.03] // common, rare, epic
};
class CrateWood extends PrimeCrateBase_1.default {
    constructor(ethNetworkConfig, overrides) {
        super('CrateWood', ethNetworkConfig, overrides);
    }
    get name() {
        return "Prime Wood Crate";
    }
    get code() {
        return "CrateWood";
    }
    get description() {
        return "Contains 1 part from the Prime collection. This crate has the highest chance to drop Common parts. More info at https://battleracers.io.";
    }
    get image() {
        return "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/CrateWoodShiny.png";
    }
    get edition() {
        return "Prime";
    }
    get rarity() {
        return "Wood";
    }
    get metaName() {
        return this.name;
    }
    get metaDescription() {
        return this.description;
    }
    get metaMarkdownDescription() {
        return "Contains 1 part from the Prime collection. This crate has the highest chance to drop Common parts. More info at [visible text](https://battleracers.io).";
    }
    get metaImage() {
        return this.image;
    }
    get metaIcon() {
        return "";
    }
    get metaExternalUrl() {
        return "";
    }
    get metaAnimationUrl() {
        return "";
    }
    get metaYoutubeUrl() {
        return "";
    }
    get metaAttributes() {
        return [
            {
                "trait_type": "Edition",
                "value": "Prime"
            },
            {
                "trait_type": "Rarity",
                "value": "Wood"
            }
        ];
    }
    /**
     * Open crate and yield parts, either pre-existing or to be minted.
     * @param partContract contract connecting to side chain -- e.g. Matic as of this moment
     * @param partsSourceAccount source wallet that owns legendary parts
     */
    async openCrate(partContract, partsSourceAccount) {
        if (!partsSourceAccount && this.partsAccount) {
            return await this._openCrate(exports.WOOD_DROP_RATE, partContract, this.partsAccount);
        }
        else if (partsSourceAccount) {
            return await this._openCrate(exports.WOOD_DROP_RATE, partContract, partsSourceAccount);
        }
        else {
            return false;
        }
    }
}
exports.default = CrateWood;
