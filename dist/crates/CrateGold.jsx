"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GOLD_DROP_RATE = void 0;
const PrimeCrateBase_1 = __importDefault(require("./PrimeCrateBase"));
exports.GOLD_DROP_RATE = {
    legendary: 0.024167,
    elite: 0.03,
    parts: 4,
    rates: [0.20, 0.50, 0.30] // common, rare, epic
};
class CrateGold extends PrimeCrateBase_1.default {
    constructor(ethNetworkConfig, overrides) {
        super('CrateGold', ethNetworkConfig, overrides);
    }
    get name() {
        return "Prime Gold Crate";
    }
    get code() {
        return "CrateGold";
    }
    get description() {
        return "Contains 4 parts from the Prime collection. This crate has the highest chance to drop Epic parts. More info at https://battleracers.io.";
    }
    get image() {
        return "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/CrateGoldShiny.png";
    }
    get edition() {
        return "Prime";
    }
    get rarity() {
        return "Gold";
    }
    get metaName() {
        return this.name;
    }
    get metaDescription() {
        return this.description;
    }
    get metaMarkdownDescription() {
        return "Contains 4 parts from the Prime collection. This crate has the highest chance to drop Epic parts. More info at [visible text](https://battleracers.io).";
    }
    get metaImage() {
        return this.image;
    }
    get metaIcon() {
        return "";
    }
    get metaExternalUrl() {
        return "";
    }
    get metaAnimationUrl() {
        return "";
    }
    get metaYoutubeUrl() {
        return "";
    }
    get metaAttributes() {
        return [
            {
                "trait_type": "Edition",
                "value": "Prime"
            },
            {
                "trait_type": "Rarity",
                "value": "Gold"
            }
        ];
    }
    /**
     * Open crate and yield parts, either pre-existing or to be minted.
     * @param partContract contract connecting to side chain -- e.g. Matic as of this moment
     * @param partsSourceAccount source wallet that owns legendary parts
     */
    async openCrate(partContract, partsSourceAccount) {
        if (!partsSourceAccount && this.partsAccount) {
            return await this._openCrate(exports.GOLD_DROP_RATE, partContract, this.partsAccount);
        }
        else if (partsSourceAccount) {
            return await this._openCrate(exports.GOLD_DROP_RATE, partContract, partsSourceAccount);
        }
        else {
            return false;
        }
    }
}
exports.default = CrateGold;
