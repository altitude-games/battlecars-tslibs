import { ChainConfigOverride, ChainNetConfig, CarPart, EthereumAddress, TokenId } from "@altitude-games/alpaca-web3";
import CrateDrawBase from "./CrateDrawBase";
export default class PrimeCrateBase extends CrateDrawBase {
    constructor(contractName: string, ethNetworkConfig: ChainNetConfig, overrides?: ChainConfigOverride);
    /**
     * Delete this function no hardcoded values are no longer necessary
     * @param partContract
     * @param sourceAccount
     */
    protected _drawLegendary(partContract: CarPart, sourceAccount: EthereumAddress): Promise<TokenId | false>;
}
