import { CarPart, ChainConfigOverride, ChainNetConfig, Crate, EthereumAddress } from "@altitude-games/alpaca-web3";
import { BattleCarPart } from "../parts";
export default class CratePoolDrawBase extends Crate {
    protected _sourceAddress: EthereumAddress | null;
    constructor(contractName: string, ethNetworkConfig: ChainNetConfig, overrides?: ChainConfigOverride);
    set partsAccount(wallet: EthereumAddress | null);
    get partsAccount(): EthereumAddress | null;
    private __drawPart;
    protected _drawParts(partContract: CarPart, partsSourceAccount: EthereumAddress, count?: number): Promise<Array<BattleCarPart>>;
}
