import { ChainNetConfig, ChainConfigOverride, CarPart, EthereumAddress } from "@altitude-games/alpaca-web3";
import { CrateEdition, CrateRarity, ICrate } from "../types";
import { BattleCarPart } from "../parts";
import CratePoolDrawBase from "./CratePoolDrawBase";
export default class CrateAxie extends CratePoolDrawBase implements ICrate {
    constructor(ethNetworkConfig: ChainNetConfig, overrides?: ChainConfigOverride);
    get name(): string;
    get code(): string;
    get description(): string;
    get image(): string;
    get edition(): CrateEdition;
    get rarity(): CrateRarity;
    get metaName(): string;
    get metaDescription(): string;
    get metaMarkdownDescription(): string;
    get metaImage(): string;
    get metaIcon(): string;
    get metaExternalUrl(): string;
    get metaAnimationUrl(): string;
    get metaYoutubeUrl(): string;
    get metaAttributes(): Array<{
        [attr: string]: string | number | boolean;
    }>;
    setErc721ContractAddress(erc721ContractAddr: EthereumAddress): Promise<void>;
    openCrate(partContract: CarPart, partsSourceAccount: EthereumAddress): Promise<Array<BattleCarPart> | false>;
}
