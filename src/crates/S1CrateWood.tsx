import { ChainNetConfig, ChainConfigOverride, CarPart, EthereumAddress } from "@altitude-games/alpaca-web3";
import { ICrate, CrateEdition, CrateRarity, DropRateConfig } from "../types";
import S1CrateBase from "./S1CrateBase";
import { BattleCarPart } from "../parts";

export const S1_WOOD_DROP_RATE:DropRateConfig = {
  legendary: 0.000043,
  elite: 0.015,
  parts: 1,
  rates: [0.80, 0.17, 0.03] // common, rare, epic
};

export default class S1CrateWood extends S1CrateBase implements ICrate {
  constructor(ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super('S1CrateWood', ethNetworkConfig, overrides);
  }

  get name():string{
    return "Season 1 Wood Crate";
  }

  get code():string{
    return "S1CrateWood";
  }

  get description():string{
    return "Contains 1 part from the Season 1 collection. This crate has the highest chance to drop Common parts. More info at https://battleracers.io.";
  }

  get image():string {
    return "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/S01Wood.png";
  }

  get edition():CrateEdition {
    return "Season 1";
  }

  get rarity():CrateRarity {
    return "Wood";
  }

  get metaName():string {
    return this.name;
  }

  get metaDescription():string {
    return this.description;
  }

  get metaMarkdownDescription():string {
    return "Contains 1 part from the Season 1 collection. This crate has the highest chance to drop Common parts. More info at [visible text](https://battleracers.io).";
  }

  get metaImage():string {
    return this.image;
  }

  get metaIcon():string {
    return "";
  }
  
  get metaExternalUrl():string {
    return "";
  }

  get metaAnimationUrl():string {
    return "";
  }

  get metaYoutubeUrl():string {
    return "";
  }

  get metaAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return [
      {
        "trait_type": "Edition", 
        "value": "Season 1"
      },
      {
        "trait_type": "Rarity", 
        "value": "Wood"
      }
    ];
  }

  /**
   * Open crate and yield parts, either pre-existing or to be minted.
   * @param partContract contract connecting to side chain -- e.g. Matic as of this moment
   * @param partsSourceAccount source wallet that owns legendary parts
   */
  public async openCrate(partContract:CarPart, partsSourceAccount:EthereumAddress):Promise<Array<BattleCarPart> | false>{
    if(!partsSourceAccount && this.partsAccount){
      return await this._openCrate(S1_WOOD_DROP_RATE, partContract, this.partsAccount);
    } else if(partsSourceAccount){
      return await this._openCrate(S1_WOOD_DROP_RATE, partContract, partsSourceAccount);
    } else {
      return false;
    }
    
  }
}