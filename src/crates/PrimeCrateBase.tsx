import { ChainConfigOverride, ChainNetConfig, CarPart, EthereumAddress, TokenId, mnemonicConverter } from "@altitude-games/alpaca-web3";
import GameMetadata from "../data/json/data.json";
import CrateDrawBase from "./CrateDrawBase";

export default class PrimeCrateBase extends CrateDrawBase {
  constructor(contractName:string, ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super(contractName, ethNetworkConfig, overrides);
    this.partsMetadataDB = GameMetadata.Parts;

    const partsMnemonic = process.env.PARTS_MNEMONIC;
    if(partsMnemonic){
      mnemonicConverter(partsMnemonic, 0, 4).then(addresses => {
        this._sourceAddress = new EthereumAddress(addresses[0]);
      });
    }
    
  }

  /**
   * Delete this function no hardcoded values are no longer necessary
   * @param partContract 
   * @param sourceAccount 
   */
  protected async _drawLegendary(partContract:CarPart, sourceAccount:EthereumAddress):Promise<TokenId | false> {
    //number of legendaries according to old code
    //does this means we only have 61 legendaries 
    //starting from token 1 to 61?
    const magicNumber = 61;  
    const remaining = await partContract.balanceOf(sourceAccount);

    if(remaining > 0){
      return magicNumber - remaining;
      
    } else {
      return false;
      
    }
  }
}