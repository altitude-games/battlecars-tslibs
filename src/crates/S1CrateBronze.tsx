import { ChainNetConfig, ChainConfigOverride, CarPart, EthereumAddress } from "@altitude-games/alpaca-web3";
import { ICrate, CrateEdition, CrateRarity, DropRateConfig } from "../types";
import S1CrateBase from "./S1CrateBase";
import { BattleCarPart } from "../parts";

export const S1_BRONZE_DROP_RATE:DropRateConfig = {
  legendary: 0.000353,
  elite: 0.015,
  parts: 2,
  rates: [0.67, 0.28, 0.05] // common, rare, epic
};

export default class S1CrateBronze extends S1CrateBase implements ICrate {
  constructor(ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super('S1CrateBronze', ethNetworkConfig, overrides);
  }

  get name():string{
    return "Season 1 Bronze Crate";
  }

  get code():string{
    return "S1CrateBronze";
  }

  get description():string{
    return "Contains 2 parts from the Season 1 collection. This crate has a high chance to drop Common parts. More info at https://battleracers.io.";
  }

  get image():string {
    return "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/S01Bronze.png";
  }

  get edition():CrateEdition {
    return "Season 1";
  }

  get rarity():CrateRarity {
    return "Bronze";
  }

  get metaName():string {
    return this.name;
  }

  get metaDescription():string {
    return this.description;
  }

  get metaMarkdownDescription():string {
    return "Contains 2 parts from the Season 1 collection. This crate has a high chance to drop Common parts. More info at [visible text](https://battleracers.io).";
  }

  get metaImage():string {
    return this.image;
  }

  get metaIcon():string {
    return "";
  }
  
  get metaExternalUrl():string {
    return "";
  }

  get metaAnimationUrl():string {
    return "";
  }

  get metaYoutubeUrl():string {
    return "";
  }

  get metaAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return [
      {
        "trait_type": "Edition", 
        "value": "Season 1"
      },
      {
        "trait_type": "Rarity", 
        "value": "Bronze"
      }
    ];
  }

  /**
   * Open crate and yield parts, either pre-existing or to be minted.
   * @param partContract contract connecting to side chain -- e.g. Matic as of this moment
   * @param partsSourceAccount source wallet that owns legendary parts
   */
  public async openCrate(partContract:CarPart, partsSourceAccount:EthereumAddress):Promise<Array<BattleCarPart> | false>{
    if(!partsSourceAccount && this.partsAccount){
      return await this._openCrate(S1_BRONZE_DROP_RATE, partContract, this.partsAccount);
    } else if(partsSourceAccount){
      return await this._openCrate(S1_BRONZE_DROP_RATE, partContract, partsSourceAccount);
    } else {
      return false;
    }
    
  }
}