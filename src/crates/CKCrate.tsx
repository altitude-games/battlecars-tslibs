import { ChainNetConfig, ChainConfigOverride, EthereumAddress, TokenId, CarPart, mnemonicConverter } from "@altitude-games/alpaca-web3";
import { CrateEdition, CrateRarity, ICrate } from "../types";
import { BattleCarPart } from '../parts';
import CratePoolDrawBase from './CratePoolDrawBase';

export default class CKCrate extends CratePoolDrawBase implements ICrate {
  constructor(ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super('CKCrate', ethNetworkConfig, overrides);

    const partsMnemonic = process.env.PARTS_MNEMONIC;
    if(partsMnemonic){
      mnemonicConverter(partsMnemonic, 0, 4).then(addresses => {
        this._sourceAddress = new EthereumAddress(addresses[3]);
      });
    }
  }

  get name():string{
    return "CryptoKitties Crate";
  }

  get code():string{
    return "CKCrate";
  }

  get description():string{
    return "CryptoKitties Crate from Battle Racers\n\n" +
      "Contains 2 parts, each from the limited edition sets of RoboKitty MK-I and RoboKitty MK-II.\n\n" +
      "More info at battleracers.io.";
  }

  get image():string {
    return "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/CrateCK.png";
  }

  get edition():CrateEdition {
    return "Cryptokitties";
  }

  get rarity():CrateRarity {
    return "";
  }

  get metaName():string {
    return this.name;
  }

  get metaDescription():string {
    return this.description;
  }

  get metaMarkdownDescription():string {
    return "CryptoKitties Crate from Battle Racers\n" +
      "Contains 2 parts, each from the limited edition sets of RoboKitty MK-I and RoboKitty MK-II.\n\n" +
      "More info at [visible text](https://battleracers.io).";
  }

  get metaImage():string {
    return this.image;
  }

  get metaIcon():string {
    return "";
  }
  
  get metaExternalUrl():string {
    return "";
  }

  get metaAnimationUrl():string {
    return "";
  }

  get metaYoutubeUrl():string {
    return "";
  }

  get metaAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return [
      {
        "trait_type": "Edition", 
        "value": "CryptoKitties"
      }
    ];
  }

  public async openCrate(partContract:CarPart, partsSourceAccount:EthereumAddress):Promise<Array<BattleCarPart> | false>{
    try {
      return await this._drawParts(partContract, partsSourceAccount, 2);
    } catch (error) {
      this._logger.error(error);
      return false;
    }
    
  }
}