import { ChainNetConfig, ChainConfigOverride, CarPart, EthereumAddress, TokenId, mnemonicConverter } from "@altitude-games/alpaca-web3";
import { CrateEdition, CrateRarity, ICrate } from "../types";
import { BattleCarPart } from "../parts";
import CratePoolDrawBase from "./CratePoolDrawBase";

export default class CrateAxie extends CratePoolDrawBase implements ICrate {
  constructor(ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super('CrateAxie', ethNetworkConfig, overrides);

    const partsMnemonic = process.env.PARTS_MNEMONIC;
    if(partsMnemonic){
      mnemonicConverter(partsMnemonic, 0, 4).then(addresses => {
        this._sourceAddress = new EthereumAddress(addresses[1]);
      });
    }
  }

  get name():string{
    return "Axie Infinity Crate";
  }

  get code():string{
    return "CrateAxie";
  }

  get description():string{
    return "Axie Infinity Crate from Battle Racers\n\n" +
      "Contains 4 parts, each from the limited edition sets of Chubby Carrot, Sleepless Bite, and Triple Nut Star.\n\n" +
      "More info at battleracers.io.";
  }

  get image():string {
    return "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/CrateAxie.png";
  }

  get edition():CrateEdition {
    return "Axie";
  }

  get rarity():CrateRarity {
    return "";
  }

  get metaName():string {
    return this.name;
  }

  get metaDescription():string {
    return this.description;
  }

  get metaMarkdownDescription():string {
    return "Axie Infinity Crate from Battle Racers\n\n" +
      "Contains 4 parts, each from the limited edition sets of Chubby Carrot, Sleepless Bite, and Triple Nut Star.\n\n" +
      "More info at [visible text](https://battleracers.io)";
  }

  get metaImage():string {
    return this.image;
  }

  get metaIcon():string {
    return "";
  }
  
  get metaExternalUrl():string {
    return "";
  }

  get metaAnimationUrl():string {
    return "";
  }

  get metaYoutubeUrl():string {
    return "";
  }

  get metaAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return [
      {
        "trait_type": "Edition", 
        "value": "Axie"
      }
    ];
  }

  async setErc721ContractAddress(erc721ContractAddr:EthereumAddress){
    this.contract.methods.setErc721ContractAddress(erc721ContractAddr).send();
  }

  public async openCrate(partContract:CarPart, partsSourceAccount:EthereumAddress):Promise<Array<BattleCarPart> | false>{
    try {
      return await this._drawParts(partContract, partsSourceAccount, 4);
    } catch(error) {
      this._logger.error(error);
      return false;
    }
    
  }
}