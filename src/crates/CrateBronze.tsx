import { ChainNetConfig, ChainConfigOverride, CarPart, EthereumAddress, TokenId } from "@altitude-games/alpaca-web3";
import { ICrate, CrateEdition, CrateRarity, DropRateConfig } from "../types";
import PrimeCrateBase from "./PrimeCrateBase";
import { BattleCarPart } from "../parts";

export const BRONZE_DROP_RATE:DropRateConfig = {
  legendary: 0.001935,
  elite: 0.03,
  parts: 2,
  rates: [0.67, 0.28, 0.05] // common, rare, epic
};

export default class CrateBronze extends PrimeCrateBase implements ICrate {
  constructor(ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super('CrateBronze', ethNetworkConfig, overrides);
  }

  get name():string{
    return "Prime Bronze Crate";
  }

  get code():string{
    return "CrateBronze";
  }

  get description():string{
    return "Prime Crate from Battle Racers\n\n" +
      "Contains 2 parts from the Prime collection. This crate has a high chance to drop Common parts.\n\n" +
      "More info at battleracers.io.";
  }

  get image():string {
    return "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/CrateBronzeShiny.png";
  }

  get edition():CrateEdition {
    return "Prime";
  }

  get rarity():CrateRarity {
    return "Bronze";
  }

  get metaName():string {
    return this.name;
  }

  get metaDescription():string {
    return this.description;
  }

  get metaMarkdownDescription():string {
    return "Prime Crate from Battle Racers\n\n" +
      "Contains 2 parts from the Prime collection. This crate has a high chance to drop Common parts.\n\n" +
      "More info at [visible text](https://battleracers.io).";
  }

  get metaImage():string {
    return this.image;
  }

  get metaIcon():string {
    return "";
  }
  
  get metaExternalUrl():string {
    return "";
  }

  get metaAnimationUrl():string {
    return "";
  }

  get metaYoutubeUrl():string {
    return "";
  }

  get metaAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return [
      {
        "trait_type": "Edition", 
        "value": "Prime"
      },
      {
        "trait_type": "Rarity", 
        "value": "Bronze"
      }
    ];
  }

  /**
   * Open crate and yield parts, either pre-existing or to be minted.
   * @param partContract contract connecting to side chain -- e.g. Matic as of this moment
   * @param partsSourceAccount source wallet that owns legendary parts
   */
  public async openCrate(partContract:CarPart, partsSourceAccount?:EthereumAddress):Promise<Array<BattleCarPart> | false>{
    if(!partsSourceAccount && this.partsAccount){
      return await this._openCrate(BRONZE_DROP_RATE, partContract, this.partsAccount);
    } else if(partsSourceAccount){
      return await this._openCrate(BRONZE_DROP_RATE, partContract, partsSourceAccount);
    } else {
      return false;
    }
  }
}