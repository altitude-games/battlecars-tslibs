import { CarPart, ChainConfigOverride, ChainNetConfig, Crate, EthereumAddress, InsufficientCarPartsError, TokenId } from "@altitude-games/alpaca-web3";
import { BattleCarPart } from "../parts";
import { ETHEREUM } from "../types";
import CarPartContractFactory from "../utils/CarPartContractFactory";

export default class CratePoolDrawBase extends Crate {
  protected _sourceAddress:EthereumAddress | null = null;

  constructor(contractName:string, ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super(contractName, ethNetworkConfig, overrides);
  }

  set partsAccount(wallet:EthereumAddress | null){
    this._sourceAddress = wallet;
  }

  get partsAccount():EthereumAddress | null {
    return this._sourceAddress;
  }

  private async __drawPart(partContract:CarPart, partsSourceAccount:EthereumAddress):Promise<TokenId> {
    this._logger.debug(`[${this.contractName}].__drawPart Drawing part from ${await partContract.tokenName()} from ${partsSourceAccount.$} balances.`);

    const sourceBalance = await partContract.balanceOf(partsSourceAccount);
    this._logger.debug(`[${this.contractName}].__drawPart ${await partContract.tokenName()} Balance: ${sourceBalance}`);

    const roll  = Math.floor(Math.random() * Number(sourceBalance));
    const tokenId = await partContract.tokenOfOwnerByIndex(partsSourceAccount, roll);

    this._logger.debug(`[${this.contractName}].__drawPart Roll: ${roll}, TokenID: ${tokenId}`);

    return tokenId;
  }

  protected async _drawParts(partContract:CarPart, partsSourceAccount:EthereumAddress, count:number = 1):Promise<Array<BattleCarPart>> {
    this._logger.debug(`[${this.contractName}].drawParts Drawing ${count} ${await partContract.tokenName()} from ${partsSourceAccount}`);

    const sourceBalance = await partContract.balanceOf(partsSourceAccount);
    if(sourceBalance === 0 || sourceBalance < count){
      throw new InsufficientCarPartsError(`Not enough tokens for drawing from ${await partContract.tokenName()} under the source account ${partsSourceAccount.$}`);
    }

    const checkList:Array<TokenId> = [];
    const drawnParts:Array<BattleCarPart> = [];
    while(drawnParts.length < count){
      const tokenDrawn = await this.__drawPart(partContract, partsSourceAccount);

      if(checkList.indexOf(tokenDrawn) < 0){
        checkList.push(tokenDrawn);
        const ethereumPartContract = CarPartContractFactory.getInstance(
          ETHEREUM, 
          this.networkConfig, //create is deployed on ethereum, we will re-use its config to request part contract instance of ethereum
          this._artifacts.overrides);
        
        const battleCarPart = new BattleCarPart(ethereumPartContract, partContract);
        await battleCarPart.loadMetadata(tokenDrawn);
        
        drawnParts.push(battleCarPart);
      }
    }
    
    return  drawnParts;
  }
}