import { CarPart, ChainConfigOverride, ChainNetConfig, Crate, EthereumAddress, TokenId } from "@altitude-games/alpaca-web3";
import { CarPartMetadataPrototype, CarPartRarity, COMMON_RARITY, DropRateConfig, EPIC_RARITY, ETHEREUM, RARE_RARITY } from "../types";
import { CarPartMetadata, LegendaryPartMarker } from "../types/CarPartMetadata";
import { BattleCarPart } from "../parts";
import CarPartContractFactory from "../utils/CarPartContractFactory";

export default class CrateDrawBase extends Crate {
  protected _sourceAddress:EthereumAddress | null = null;

  protected _partsMetadataDB:Array<any>;

  constructor(contractName:string, ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super(contractName, ethNetworkConfig, overrides);
    this._partsMetadataDB = [];
  }

  set partsAccount(wallet:EthereumAddress | null){
    this._sourceAddress = wallet;
  }

  get partsAccount():EthereumAddress | null {
    return this._sourceAddress;
  }

  set partsMetadataDB(partsMetadataDB:Array<any>) {
    this._partsMetadataDB = partsMetadataDB;
  }

  get partsMetadataDB():Array<any> {
    return this._partsMetadataDB;
  }

  protected _getLegendaryRate(config:DropRateConfig) {
    const now = Date.now();

    let legRate = config.legendary;

    this._logger.debug(`\tLEGENDARY_BONUS_START = ${process.env.LEGENDARY_BONUS_START}`);
    this._logger.debug(`\LEGENDARY_BONUS_END = ${process.env.LEGENDARY_BONUS_END}`);
    this._logger.debug(`\LEGENDARY_MULTIPLIER = ${process.env.LEGENDARY_MULTIPLIER}`);

    if(process.env.LEGENDARY_BONUS_START && process.env.LEGENDARY_BONUS_END && process.env.LEGENDARY_MULTIPLIER){
      this._logger.info(`Legendary bonus config is found.`);

      const bonusStart = Number(process.env.LEGENDARY_BONUS_START);
      const bonusEnd = Number(process.env.LEGENDARY_BONUS_END);
      const multiplier = Number(process.env.LEGENDARY_MULTIPLIER);

      if (now >= bonusStart && now < bonusEnd) {
        this._logger.info(`Legendary bonus config is in effect.`);
        legRate *= multiplier;
      } else {
        this._logger.info(`Legendary bonus config has finished.`);
      }
    } else {
      this._logger.info(`No legendary bonus override from ENV`);
    }

    return legRate;
  }

  protected _getPartsByRarity(rarity:CarPartRarity):Array<CarPartMetadataPrototype> {
    if(!this._cache.partsdb){
      this._cache.partsdb = {};
    }

    if(!this._cache.partsdb[rarity]){
      this._cache.partsdb[rarity] = this.partsMetadataDB.filter((part:any) => part.rarity.toLowerCase() === rarity.toLowerCase());
    }

    return this._cache.partsdb[rarity];
  }

  protected _drawPart(rarity:CarPartRarity):CarPartMetadata{
    const partsSelection = this._getPartsByRarity(rarity);

    let max = partsSelection.length,
        roll  = Math.floor(Math.random() * (max)),
        part = partsSelection[roll],
        durRoll = Math.floor(Math.random() * (11)),
        spdRoll = Math.floor(Math.random() * (11)),
        powRoll = Math.floor(Math.random() * (11)),
        strRoll = Math.floor(Math.random() * (11)),

        calcStat = (stat:number, roll:number) => {
          return Math.floor(stat * Number((roll / 100 + 0.9).toPrecision(2)));
        };

    let imageUrlPrefix = process.env.IMAGE_URL || process.env.PARTS_IMAGE_URL;

    if(!imageUrlPrefix){
      imageUrlPrefix = "http://localhost/parts/images/";
      this._logger.warn(`IMAGE_URL or PARTS_IMAGE_URL prefixes are not specified on environment variables, using default ${imageUrlPrefix}`);
    }

    return {
      name: part.name,
      description: part.desc,
      image: `${imageUrlPrefix}${part.id}.png`,
      
      details: {
        internalId: part.id,
        edition: part.edition,
        brand: part.brand,
        model: part.model,
        internalType: part.type,
        type: part.type_ext,
        rarity: part.rarity,
        weight: part.weight,
        durability: calcStat(part.durability, durRoll),
        speed: calcStat(part.speed, spdRoll),
        power: calcStat(part.power, powRoll),
        steering: calcStat(part.steering, strRoll),
      }
    };
  }

  protected _performCrateDraw(config:DropRateConfig, hasLegendaries:boolean):Array<CarPartMetadata | LegendaryPartMarker>{
    const drawnParts:Array<CarPartMetadata | LegendaryPartMarker> = [];
    const effLegendaryRate = this._getLegendaryRate(config);

    const gotLegendary:boolean = (Math.random() < effLegendaryRate) && hasLegendaries;
    const thresholds:number[] = [];

    let lastThreshold = 0;
    for (let i = 0; i < config.rates.length; i++) {
      const threshold = lastThreshold + config.rates[i];
      thresholds.push(Number((threshold).toPrecision(2)));
      lastThreshold = threshold;
    }

    for (let i = 0; i < config.parts; i++) {
      const isElite = (Math.random() < config.elite);
      const rarityRoll = Math.random();
      let part:CarPartMetadata | null = null;
  
      [COMMON_RARITY, RARE_RARITY, EPIC_RARITY].some((rarity, index) => {
        if (rarityRoll < thresholds[index]) {
          part = this._drawPart(rarity);
          
          part.details = { ...part.details, isElite };

          this._logger.debug("\tPart # %d : %s; Elite? %s", i, part.details.rarity, isElite);

          drawnParts.push(part);

          return true; //break here
        }

        return false;
      });
    }

    //got lucky ?
    this._logger.info(`\tDraw Got Legendary? ${gotLegendary}`);
    if (gotLegendary) {
      drawnParts.push({isLegendary: true});
    }

    return drawnParts;
  }

  protected async _drawLegendary(partContract:CarPart, sourceAccount:EthereumAddress):Promise<TokenId | false> {
    let partObtained:TokenId|false = false;

    const remaining = await partContract.balanceOf(sourceAccount);
    if(remaining > 0){
      const legendaryRoll = Math.floor(Math.random() * remaining);
      partObtained = await partContract.tokenOfOwnerByIndex(sourceAccount, legendaryRoll);
    }

    return partObtained;
  }

  /**
   * Open crate and yield parts, either pre-existing or to be minted.
   * @param config drop rate configuration
   * @param partContract contract connecting to side chain -- e.g. Matic as of this moment
   * @param partsSourceAccount source wallet that owns legendary parts
   */
  protected async _openCrate(config:DropRateConfig, partContract:CarPart, partsSourceAccount:EthereumAddress):Promise<Array<BattleCarPart>>{
    const result = new Array<BattleCarPart>();

    const reserved = await partContract.balanceOf(partsSourceAccount);
    
    const parts = this._performCrateDraw(config, reserved > 0);
    const ethereumPartContract = CarPartContractFactory.getInstance(
      ETHEREUM, 
      this.networkConfig, //create is deployed on ethereum, we will re-use its config to request part contract instance of ethereum
      this._artifacts.overrides);

    for(let i = 0; i < parts.length; i++){
      const part = parts[i];

      let battleCarPart:BattleCarPart;

      if(Object.keys(part).indexOf('isLegendary') > 0){
        const partObtained = await this._drawLegendary(partContract, partsSourceAccount);
        if(partObtained){
          battleCarPart = new BattleCarPart(ethereumPartContract, partContract, partObtained);
          result.push(battleCarPart);
        }
      } else {
        //minting new part, only legendary picks are pre-minted
        if((part as CarPartMetadata).id){
          delete((part as CarPartMetadata).id);
        }

        (part as CarPartMetadata).provisioningId = `${Date.now()}${Math.round(Math.random() * 1000000).toString().padStart(7, "0")}`;

        battleCarPart = new BattleCarPart(ethereumPartContract, partContract);
        battleCarPart.metadata = part;

        result.push(battleCarPart);
      }
    }
    
    return result;
  }
}