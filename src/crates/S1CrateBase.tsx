import { CarPart, ChainConfigOverride, ChainNetConfig, EthereumAddress, mnemonicConverter, TokenId } from "@altitude-games/alpaca-web3";
import GameMetadata from "../data/json/s1_data.json";
import CrateDrawBase from "./CrateDrawBase";

export default class S1CrateBase extends CrateDrawBase {
  constructor(contractName:string, ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super(contractName, ethNetworkConfig, overrides);
    this.partsMetadataDB = GameMetadata.Parts;

    const partsMnemonic = process.env.PARTS_MNEMONIC;
    if(partsMnemonic){
      mnemonicConverter(partsMnemonic, 0, 4).then(addresses => {
        this._sourceAddress = new EthereumAddress(addresses[2]);
      });
    }
  }

  /**
   * Delete this function no hardcoded values are no longer necessary
   * @param partContract 
   * @param sourceAccount 
   */
  protected async _drawLegendary(partContract:CarPart, sourceAccount:EthereumAddress):Promise<TokenId | false> {
    let partObtained:TokenId|false = false;

    const magicNumber = 20; //number of legendaries
    const remaining = await partContract.balanceOf(sourceAccount);

    if(remaining > 0){
      const diff = magicNumber - remaining;
      const startTokenId = Number(process.env.S1_LEG_START);
      return startTokenId + diff;

    } else {
      return false;

    }

    return partObtained;
  }
}