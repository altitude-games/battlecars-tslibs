import AWS, {DynamoDB} from 'aws-sdk';
import Bunyan from 'bunyan';
import { EthereumAddress, TokenId } from '@altitude-games/alpaca-web3';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { ChainType, ETHEREUM, NO, PART_STATE_READY, PART_STATE_TRANSIT, SIDECHAIN, YES } from '../../types';

if(!process.env.DYNAMO_TABLE_NAMESPACE || !process.env.AWS_ACCESS_KEY_ID || !process.env.AWS_SECRET_ACCESS_KEY){
  require('dotenv').config();
}

const NAMESPACE = process.env.DYNAMO_TABLE_NAMESPACE || "";
export const METADATA_TABLE = NAMESPACE + "TokenMetadata";
export const ACCOUNTS_TABLE = NAMESPACE + "Accounts";
export const BATTLERACERS_TABLE = NAMESPACE + "BattleRacers";
export const SERIALS_TABLE = NAMESPACE + "SerialNumbers";

class DynamoDBInterface {
  private __logger:Bunyan;
  private __awsService:DynamoDB;
  private __docClient:DynamoDB.DocumentClient;

  constructor(){
    this.__logger = Bunyan.createLogger({name: 'dynamodb', level: (process.env.NODE_ENV === "development" ? "debug" : "info")});

    const AWS_REGION = process.env.AWS_REGION || 'ap-southeast-1';
    const AWS_API_VERSION = process.env.AWS_API_VERSION || '2012-08-10';
    const AWS_ACCESS_KEY_ID = process.env.AWS_ACCESS_KEY_ID;
    const AWS_SECRET_ACCESS_KEY = process.env.AWS_SECRET_ACCESS_KEY;

    const dynamoDbOpts:any = { region: AWS_REGION, apiVersion: AWS_API_VERSION, accessKeyId: AWS_ACCESS_KEY_ID, secretAccessKey: AWS_SECRET_ACCESS_KEY };

    if(process.env.DYNAMO_ENDPOINT){
      dynamoDbOpts.endpoint = process.env.DYNAMO_ENDPOINT;
    
      this.__logger.info("Using development DynamoDB Connection", process.env.DYNAMO_ENDPOINT);
    } else {
      this.__logger.info("Using a live DynamoDB Connection");
    }

    this.__awsService = new AWS.DynamoDB(dynamoDbOpts);
    this.__docClient = new AWS.DynamoDB.DocumentClient({service: this.__awsService});
  }

  get service():DynamoDB {
    return this.__awsService;
  }

  get client():DynamoDB.DocumentClient {
    return this.__docClient;
  }

  public async getMetadata(tokenIds:Array<TokenId>|TokenId, raw:boolean = false) {
    const metadataIds:Array<TokenId> = tokenIds instanceof Array ? tokenIds : [tokenIds];

    if (metadataIds.length === 0) {
      return [];
    }

    const params:any = {
      RequestItems: {}
    }

    params.RequestItems[METADATA_TABLE] = {
      Keys: []
    };

    if (!raw) {
      this.__logger.info("getMetadata not raw");

      params.RequestItems[METADATA_TABLE].ExpressionAttributeNames = {
        "#name": "name",
        "#state": "state"
      };
      
      params.RequestItems[METADATA_TABLE].ProjectionExpression = "id, #name, description, details, image, external_url, chain, #state";
    }

    for (var i = 0; i < metadataIds.length; i++) {
      params.RequestItems[METADATA_TABLE].Keys.push({id: metadataIds[i]});
    }

    const resp = await this.client.batchGet(params).promise();
    const items = !resp.Responses ? [] : resp.Responses[METADATA_TABLE];
    
    return metadataIds.length == 1 ? (items.length > 0 ? items[0] : false) : items;
  }

  public putMetadata(jsonData:DocumentClient.AttributeMap) {
    var params = {
      TableName: METADATA_TABLE,
      Item: jsonData
    }

    return this.client.put(params).promise();
  }

  public async getSerialNum(id:string) {
    var params = {
      TableName: SERIALS_TABLE,
      Key: { id }
    }
    var resp = await this.client.get(params).promise();
    var item = resp.Item;

    return (item && item.count) || "0";
  }

  public async createMetadata(jsonData:DocumentClient.AttributeMap, serialPartId:string, count:string | number) {
    var params:any = {RequestItems: {}};
    params.RequestItems[SERIALS_TABLE] = [
      {
        PutRequest: {
          Item: {
            id: serialPartId,
            count: count
          }
        }
      }
    ];
    params.RequestItems[METADATA_TABLE] = [
      {
        PutRequest: {
          Item: jsonData
        }
      }
    ];

    return this.client.batchWrite(params).promise();
  }

  public async commitMetadataOwner(id:TokenId) {
    var params = {
      TableName: METADATA_TABLE,
      Key: { id },
      UpdateExpression: "SET #owner = transferTo, transferComplete = :isComplete, #state = :state REMOVE pendingTxnHash",
      ExpressionAttributeNames: {
        "#state": "state",
        "#owner": "owner"
      },
      ExpressionAttributeValues: {
        ':isComplete': YES,
        ':state': PART_STATE_READY
      }
    };

    return this.client.update(params).promise();
  }

  public async getMetadataOwnedCount(owner:EthereumAddress, chain:ChainType) {
    var params:any = {
      TableName: METADATA_TABLE,
      IndexName: "owner-chainSortId-index",
      ReturnConsumedCapacity: "INDEXES",
      KeyConditionExpression: "#owner = :owner",
      ExpressionAttributeNames: {
        "#owner": "owner"
      },
      ExpressionAttributeValues: {
        ':owner': owner.$
      },
      Select: 'COUNT'
    };

    if (chain !== "ALL") {
      params.FilterExpression = "chain = :chain";
      params.ExpressionAttributeValues[":chain"] = chain;
    }

    var queryResult = await this.client.query(params).promise();

    return queryResult.Count;
  }

  public async updateAccountPartsCount(ethWallet:EthereumAddress) {
    const sideChainCount = await this.getMetadataOwnedCount(ethWallet, SIDECHAIN);
    const mainChainCount = await this.getMetadataOwnedCount(ethWallet, ETHEREUM);

    var params = {
      TableName: ACCOUNTS_TABLE,
      Key: { ethWallet: ethWallet.$ },
      UpdateExpression: "set sideChainParts = :sideChainParts, mainChainParts = :mainChainParts",
      ExpressionAttributeValues: {
        ':sideChainParts': sideChainCount,
        ':mainChainParts': mainChainCount
      },
      ReturnValues: "UPDATED_NEW"
    }

    await this.client.update(params).promise();

    return {sideChainCount, mainChainCount};
  }

  /**
   * Set
   *  txHash = 0 if state should not be altered.
   *  txHash = null if state needs to be cleared as READY
   *  txHash = actual transaction hash if needs to be monitored as pending
   */
  public async prepMetadataTransfer(id:TokenId, owner:EthereumAddress, crateType?:string, txHash?:string | number | null) {
    var params:any = {
      TableName: METADATA_TABLE,
      Key: {id},
      UpdateExpression: "set transferTo = :owner, transferTime = :transferTime",
      ExpressionAttributeValues: {
        ':owner': owner.$,
        ':transferTime': Date.now()
      }
    };

    if(crateType){
      params.UpdateExpression = `${params.UpdateExpression}, crateBox = :crateBox`;
      params.ExpressionAttributeValues[':crateBox'] = crateType;
    }

    if(txHash && txHash !== 0){
      params.UpdateExpression = `${params.UpdateExpression}, pendingTxnHash = :txHash, transferComplete = :isComplete, #state = :state`;
      params.ExpressionAttributeValues[':txHash'] = txHash;
      params.ExpressionAttributeValues[':isComplete'] = NO;  //will be updated by partsTransferDaemon
      params.ExpressionAttributeValues[':state'] = PART_STATE_TRANSIT;
      params.ExpressionAttributeNames = {
        '#state': 'state'
      };

    } else if(txHash === null) {
      params.UpdateExpression = `${params.UpdateExpression}, pendingTxnHash = :txHash, transferComplete = :isComplete, #state = :state, #owner = :owner`;

      params.ExpressionAttributeValues[':isComplete'] = YES;
      params.ExpressionAttributeValues[':txHash'] = "";
      params.ExpressionAttributeValues[':state'] = PART_STATE_READY;

      params.ExpressionAttributeNames = {
        '#state': 'state',
        '#owner': "owner"
      };

    } else { //txHash == 0
      params.UpdateExpression = `${params.UpdateExpression}, #owner = :owner`;
      params.ExpressionAttributeNames = {
        '#owner': "owner"
      };
    }

    return this.client.update(params).promise();
  }
}

export default class AWSDynamoDB {
  static instance:DynamoDBInterface;

  constructor() {
    throw new Error('Use DynamoDB.getInstance()');
  }

  static getInstance() {
      if (!AWSDynamoDB.instance) {
        AWSDynamoDB.instance = new DynamoDBInterface();
      }
      return AWSDynamoDB.instance;
  }
}