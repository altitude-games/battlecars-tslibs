export default interface Stats {
  weight: number;
  durability?: number;
  speed?: number;
  power?: number;
  steering?: number;
}