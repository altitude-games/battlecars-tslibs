import { IERC20 } from "@altitude-games/alpaca-web3";
import { BasicCrateInfo, IOpenseaMetadata } from ".";

export default interface ICrate extends BasicCrateInfo, IOpenseaMetadata, IERC20 {
  
}