export { default as Stats } from "./Stats";
export { default as BasicCrateInfo } from "./BasicCrateInfo";
export { default as ICrate } from "./ICrate";
export { default as IOpenseaMetadata } from "./opensea/ICrateMetadata";

export type CrateRarity = "Bronze" | "Gold" | "Silver" | "Wood" | "";
export type CrateEdition = "Cryptokitties" | "Axie" | "Prime" | "Season 1";
export type CarPartType = "Body" | "Front" | "Rear" | "Wheels";
export type CarInternalPartType = "Casing" | "Bumper" | "Spoiler" | "Wheels";
export type CarPartRarity = "Common" | "Epic" | "Rare" | "Legendary";
export type CarPartEdition = "Special" | "Prime" | "Season 1";

export const COMMON_RARITY:CarPartRarity = "Common";
export const RARE_RARITY:CarPartRarity = "Rare";
export const EPIC_RARITY:CarPartRarity = "Epic";
export const LEGENDARY_RARITY:CarPartRarity = "Legendary";

export type PartState = "READY" | "TRANSIT" | "ETH_AWAIT_CLAIM" | "ETH_CLAIM_READY" | "CLAIMING";
export const PART_STATE_READY:PartState = "READY";
export const PART_STATE_TRANSIT:PartState = "TRANSIT";
export const PART_STATE_ETH_AWAIT_CLAIM:PartState = "ETH_AWAIT_CLAIM";
export const PART_STATE_ETH_CLAIM_READY:PartState = "ETH_CLAIM_READY";
export const PART_STATE_CLAIMING:PartState = "CLAIMING";

export type FlagType = "yes" | "no" | true | false;
export const YES:FlagType = "yes";
export const NO:FlagType = "no";

export type ChainType = "ETH" | "LOOM" | "ALL";
export const ETHEREUM:ChainType = "ETH";
export const SIDECHAIN:ChainType = "LOOM";
export const ALLCHAIN:ChainType = "ALL";

export interface DropRateConfig {
  legendary: number;
  elite: number;
  parts: number;
  rates: Array<number>;
}

export interface CarPartMetadataPrototype {
  id:string;
  name:string;
  desc:string;
  type:string;
  type_ext:string;
  gltf:string;
  brand:string;
  model:string;
  edition:CarPartEdition;
  rarity:CarPartRarity;
  weight:number;
  durability:number;
  speed:number;
  power:number;
  steering:number;
}