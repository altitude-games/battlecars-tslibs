import Web3 from "web3";
import Bunyan from "bunyan";
import { CarPart, EthereumAddress, TokenId } from "@altitude-games/alpaca-web3";
import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { AWSDynamoDB } from "../data";
import { CarPartMetadata, CarPartMetadataDetails } from "../types/CarPartMetadata";
import { ChainType, ETHEREUM, NO, PART_STATE_READY, PART_STATE_TRANSIT, SIDECHAIN, YES  } from "../types";


export default class BattleCarPart implements CarPartMetadata {
  private __dynamodb;
  private __mainContract:CarPart;
  private __sideContract:CarPart;
  private __metadata:DocumentClient.AttributeMap | false = false;
  protected _logger:Bunyan;

  constructor(mainContract:CarPart, sideContract:CarPart, carPartId?:TokenId){
    this._logger = Bunyan.createLogger({
      name: `BattleCarPart`,
      level: process.env.NODE_ENV === 'development' ? Bunyan.DEBUG : Bunyan.INFO
    });

    this.__mainContract = mainContract;
    this.__sideContract = sideContract;
    this.__dynamodb = AWSDynamoDB.getInstance();

    if(carPartId){
      this.__dynamodb.getMetadata(carPartId, true)
        .then(result => {
          this.__metadata = result;
        })
        .catch(error => {
          throw error;
        });
    }
    
  }

  async balanceOf(wallet:EthereumAddress, chain:ChainType = SIDECHAIN){
    return chain === ETHEREUM ? await this.__mainContract.balanceOf(wallet) : await this.__sideContract.balanceOf(wallet);
  }

  get id():TokenId {
    return this.__metadata ? this.__metadata.id : 0;
  }

  get name():string {
    return this.__metadata ? this.__metadata.name : "";
  }

  get description(): string {
    return this.__metadata ? this.__metadata.description : "";
  }

  get image(): string {
    return this.__metadata ? this.__metadata.image : "";
  }

  get external_url(): string {
    return this.__metadata ? this.__metadata.external_url : "";
  }

  get details():CarPartMetadataDetails {
    return this.__metadata ? this.__metadata.details : {}; 
  }

  get metadata():DocumentClient.AttributeMap | false {
    return this.__metadata;
  }

  set metadata(metadata:DocumentClient.AttributeMap | false) {
    this.__metadata = metadata;
  }

  public async loadMetadata(tokenId:TokenId){
    return this.__metadata = await this.__dynamodb.getMetadata(tokenId, true);
  }

  public async isVerifiedOwner():Promise<boolean> {
    if(this.__metadata && this.__metadata.owner && this.__metadata.id){
      const dbRecordedOwner = new EthereumAddress(this.__metadata.owner);
      const chainRecognizedOwner = await this.__sideContract.ownerOf(this.__metadata.id);

      const result = dbRecordedOwner.equals(chainRecognizedOwner);
      if(!result){
        this._logger.debug(`Part token ${this.__metadata.id} owner discrepency. DB: ${dbRecordedOwner}, Network: ${chainRecognizedOwner}`);
      }

      return result;

    } else {
      return false;
    }
    
  }

  public async save(destinationWallet:EthereumAddress, crateBox:string = ''){
    this._logger.debug(`Saving part metadata`, this.__metadata, destinationWallet);

    this._logger.debug("addresses", this.__sideContract.addresses);

    if(this.__metadata){
      if(!this.id || this.id == 0){
        this._logger.debug(`\tFresh minting part...`);

        const PORT = process.env.API_PORT || 3006;
        const METADATA_URL = process.env.METADATA_URL || 'http://localhost:' + PORT + "/api/items/";
        const EXTERNAL_URL = process.env.EXTERNAL_URL || "http://localhost:" + PORT + "/items/";

        //mint 
        const totalSupply = await this.__sideContract.totalSupply();
        const tokenId = Web3.utils.toBN(totalSupply).add(Web3.utils.toBN(1)).toString();
        const tokenURI = `${METADATA_URL}${tokenId}`;

        await this.__sideContract.mintPart(destinationWallet, tokenId, tokenURI, {
          'transactionHash': async (txHash:string) => {
            const part = this.__metadata || {};
            const sortId = Web3.utils.toBN(tokenId).toString(16, 64);
            const partIdSerial = `${part.details.brand}${part.details.model}${part.details.rarity}-${part.details.internalType}`;
            const serial = Web3.utils.toBN(await this.__dynamodb.getSerialNum(partIdSerial));
            const newSerial = serial.add(Web3.utils.toBN(1)).toString();

            this.__metadata = {
              ...this.__metadata,
              id: tokenId,
              owner: destinationWallet.$,
              chain: SIDECHAIN,
              state: PART_STATE_TRANSIT,
              sortId,
              chainSortId: `${SIDECHAIN}_${sortId}_${PART_STATE_TRANSIT}`,
              external_url: `${EXTERNAL_URL}${tokenId}`,
              crateBox,
              transferTo: destinationWallet.$,
              pendingTxnHash: txHash,
              transferComplete: NO,
              details: {
                ...this.__metadata ? this.__metadata.details : {},
                serialNumber: newSerial,
                birthTxnHash: txHash,
              }
            };

            await this.__dynamodb.createMetadata(this.__metadata, partIdSerial, newSerial);
          }

        }).catch(reason => {
          this._logger.info(`Part minting transaction failed!`, reason);
          throw reason;
        });

      } else {
        this._logger.debug(`\tTransferring part from reserve account...`);

        const metadataOwner = new EthereumAddress(this.__metadata.owner);
        this._logger.debug(`\tMetadata Owner`, metadataOwner);

        const isOwnershipConsistent = await this.isVerifiedOwner();

        if(!isOwnershipConsistent){
          this._logger.info(`\tBattle Car Part ${this.__metadata.id} is being transferred to new owner ${destinationWallet} but the source account from DB does not match with network's actual owner`);
          throw new Error(`Battle Car Part ${this.__metadata.id} is being transferred to new owner ${destinationWallet} but the source account from DB does not match with network's actual owner`);
        }

        //set sender as source account
        this.__sideContract.networkOverrides = { 
          ...this.__sideContract.networkOverrides, 
          options: { 
            ...this.__sideContract.networkOverrides?.options, 
            from: metadataOwner.$  //ensure that this account belongs to mnemonic used to initiate the contract
          } 
        };

        const operator = (this.__sideContract.networkOverrides.options?.from && new EthereumAddress(this.__sideContract.networkOverrides.options.from)) || await this.__sideContract.getPrimaryAccount();

        if(!metadataOwner.equals(operator)){
          const canTransfer = await this.__sideContract.isApprovedForAll(metadataOwner, operator);

          if(!canTransfer){
            this._logger.info(`\tPrimary account ${operator.$} is not approved for ${this.__metadata.owner} 's assets!`);
            throw new Error(`Primary account ${operator.$} is not approved for ${this.__metadata.owner} 's assets. Transfer will not continue.`);
          } 
        }

        await this.__sideContract.transferTokenFrom(metadataOwner, destinationWallet, this.__metadata.id, {
          'transactionHash': async (txHash:string) => {

            await this.__dynamodb.prepMetadataTransfer(
              this.metadata && this.metadata.id, 
              destinationWallet,
              crateBox,
              txHash);

            /* this.__metadata = {
              ...this.__metadata,
              state: PART_STATE_TRANSIT,
              crateBox,
              transferTo: destinationWallet.$,
              pendingTxnHash: txHash,
              transferComplete: NO
            };

            await this.__dynamodb.putMetadata(this.__metadata); */

            //update metadata from db
            await this.loadMetadata(this.metadata && this.metadata.id);
          },
          'then': this.__onPartTransactionCompleted.bind(this),
          
        }).catch(reason => {
          this._logger.info(`Part transfer transaction failed!`, reason);
          throw reason;
        });
        
      }
    } 
  }

  private async __onPartTransactionCompleted(txReceipt:any){
    this._logger.info(`Part transaction completed, fully commiting ownership.`);
    
    const oldOwner = (this.metadata as DocumentClient.AttributeMap).owner as string;
    const newOwner = (this.metadata as DocumentClient.AttributeMap).transferTo as string;

    this._logger.debug(`Last owner -> New owner`, oldOwner, newOwner);

    /* this.__metadata = {
      ...this.__metadata,
      state: PART_STATE_READY,
      owner: (this.__metadata as DocumentClient.AttributeMap).transferTo,
      transferComplete: YES
    };

    if(this.__metadata.pendingTxnHash)
      delete(this.__metadata.pendingTxnHash); */

    this._logger.debug(`DB commit.`, this.metadata && this.metadata.id);
    await this.__dynamodb.commitMetadataOwner(this.metadata && this.metadata.id);
    
    //update metadata from db
    this._logger.debug(`Load metadata`);
    await this.loadMetadata(this.metadata && this.metadata.id);
    
    //update parts count cache for old owner account
    //if different from new owner
    if(oldOwner.toLowerCase() !== newOwner.toLowerCase()){
      this._logger.debug(`Updating parts count for oldOwner`, oldOwner);
      await this.__dynamodb.updateAccountPartsCount(new EthereumAddress(oldOwner)); 
    }

    //update parts count cache for new owner account
    this._logger.debug(`Updating parts count for newOwner`, newOwner);
    await this.__dynamodb.updateAccountPartsCount(new EthereumAddress(newOwner));

    this._logger.debug(`\tCompleted metadata commit:`, this.__metadata);
  }
}